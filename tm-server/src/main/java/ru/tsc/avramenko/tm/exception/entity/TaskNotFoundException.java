package ru.tsc.avramenko.tm.exception.entity;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}