package ru.tsc.avramenko.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.ISessionRepository;
import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.api.repository.IUserRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.exception.system.ProcessException;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new ProcessException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ProcessException();
        @Nullable final String password = propertyService.getJdbcPass();
        if (password == null) throw new ProcessException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ProcessException();

        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}